// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A simple Stokes test problem for the staggered grid (Navier-)Stokes model.
 */

#ifndef DUMUX_STOKES1P2C_RANS_SUBPROBLEM_HH
#define DUMUX_STOKES1P2C_RANS_SUBPROBLEM_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>

#include <dumux/material/fluidsystems/1padapter.hh>
#include <dumux/material/H2OAirIsotopes.hh>


#include <dumux/freeflow/compositional/komegancmodel.hh>
#include <dumux/freeflow/rans/twoeq/komega/problem.hh>
#include <dumux/freeflow/turbulenceproperties.hh>

#include <dumux/freeflow/navierstokes/boundarytypes.hh>


namespace Dumux {
template <class TypeTag>
class StokesSubProblem;

namespace Properties {
// Create new type tags
namespace TTag {
#if NONISOTHERMAL
struct StokesOnePTwoC { using InheritsFrom = std::tuple<KOmegaNCNI, StaggeredFreeFlowModel>; };
 #else
struct StokesOnePTwoC { using InheritsFrom = std::tuple<KOmegaNC, StaggeredFreeFlowModel>; };
#endif
} // end namespace TTag


// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::StokesOnePTwoC> { using type = Dune::YaspGrid<2, Dune::TensorProductCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2> >; };

// The fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::StokesOnePTwoC>
{
    using H2OAirIsotopes = FluidSystems::H2OAirIsotopes<GetPropType<TypeTag, Properties::Scalar>>;
    static constexpr auto phaseIdx = H2OAirIsotopes::gasPhaseIdx; // simulate the water phase
    using type = FluidSystems::OnePAdapter<H2OAirIsotopes, phaseIdx>;
};

template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::StokesOnePTwoC> { static constexpr int value = GetPropType<TypeTag, Properties::FluidSystem>::numComponents; };

// Use formulation based on mass fractions
template<class TypeTag>
struct UseMoles<TypeTag, TTag::StokesOnePTwoC> { static constexpr bool value = true; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::StokesOnePTwoC> { using type = Dumux::StokesSubProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::StokesOnePTwoC> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::StokesOnePTwoC> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::StokesOnePTwoC> { static constexpr bool value = true; };
} // end namespace Properties

    /*!
     * \ingroup BoundaryTests
     * \brief Test problem for the one-phase (Navier-) Stokes problem.
     *
     * Horizontal flow from left to right with a parabolic velocity profile.
     */
template <class TypeTag>
class StokesSubProblem : public RANSProblem<TypeTag>
{
    using ParentType = RANSProblem<TypeTag>;

    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag,Properties::ModelTraits>::numEq()>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using ElementVolumeVariables = typename GetPropType<TypeTag,Properties::GridVolumeVariables>::LocalView;
    using ElementFaceVariables = typename GetPropType<TypeTag, Properties::GridFaceVariables>::LocalView;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

        using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
        //     using TimeLoopPtr = std::shared_ptr<CheckPointTimeLoop<Scalar>>;
        using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;

        static constexpr auto dimWorld = GridGeometry::GridView::dimensionworld;

        using DiffusionCoefficientAveragingType = typename StokesDarcyCouplingOptions::DiffusionCoefficientAveragingType;

        static constexpr bool useMoles = GetPropType<TypeTag, Properties::ModelTraits>::useMoles();

        using H2O = Components::TabulatedComponent<Components::H2O<Scalar> >;
        using Air = Dumux::Components::Air<Scalar>;
        using HDO = Dumux::Components::HDO<Scalar>;
        using H2O18 = Dumux::Components::H2O18<Scalar>;

    public:
        StokesSubProblem(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<CouplingManager> couplingManager)
        : ParentType(gridGeometry, "Stokes"), eps_(1e-6), couplingManager_(couplingManager)
        {
            refVelocity_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.RefVelocity");
            refPressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.RefPressure");
            refTemperature_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.RefTemperature");
            relHumidity_=getParamFromGroup<Scalar>(this -> paramGroup(),"Problem.relHumidity");
            fracLiqD_= getParamFromGroup<Scalar>(this->paramGroup(), "Problem.FracLiqD");
            fracLiqO18_= getParamFromGroup<Scalar>(this->paramGroup(), "Problem.FracLiqO18");

            diffCoeffAvgType_ = StokesDarcyCouplingOptions::stringToEnum(DiffusionCoefficientAveragingType{},getParamFromGroup<std::string>(this->paramGroup(), "Problem.InterfaceDiffusionCoefficientAvg"));
            problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

            //RANS Equation
            FluidSystem::init();
            Dumux::TurbulenceProperties<Scalar, dimWorld, true> turbulenceProperties;
            FluidState fluidState;
            const auto phaseIdx = 0;
            fluidState.setPressure(phaseIdx, 1e5);
            fluidState.setTemperature(refTemperature());
            fluidState.setMoleFraction(phaseIdx, 1/*FluidSystem::AirIdx*/, 1.0);
            fluidState.setMoleFraction(phaseIdx, 0/*FluidSystem::H2OIdx*/, 0.0);
            Scalar density = FluidSystem::density(fluidState, 0);
            Scalar kinematicViscosity = FluidSystem::viscosity(fluidState, phaseIdx) / density;
            Scalar diameter = this->gridGeometry().bBoxMax()[1] - this->gridGeometry().bBoxMin()[1];

            turbulentKineticEnergy_ = turbulenceProperties.turbulentKineticEnergy(refVelocity_, diameter, kinematicViscosity);

            dissipation_ = turbulenceProperties.dissipationRate(refVelocity_, diameter, kinematicViscosity);

            initializationTime_ = getParam<Scalar>("TimeLoop.Initialization", -1.0);

            turbulenceModelName_ = turbulenceModelToString(ModelTraits::turbulenceModel());
            std::cout << "Using the "<< turbulenceModelName_ << " Turbulence Model. \n";
            std::cout << std::endl;
        }

        template<class SolutionVector, class GridVariables>
        void printWaterMass(const SolutionVector& curSol,
                            const GridVariables& gridVariables,
                            const Scalar timeStepSize)

        {
            // compute the mass in the entire domain
            Scalar evaporation = 0.0;
            Scalar evaporationHDO = 0.0;
            Scalar evaporationH2O18 = 0.0;
            Scalar energyFlux = 0.0;


            if (!(time() < 0.0))
            {

                // bulk elements
                for (const auto& element : elements(this->gridGeometry().gridView()))
                {
                    auto fvGeometry = localView(this->gridGeometry());
                    fvGeometry.bindElement(element);

                    auto elemVolVars = localView(gridVariables.curGridVolVars());
                    elemVolVars.bindElement(element, fvGeometry, curSol);

                    auto elemFaceVars = localView(gridVariables.curGridFaceVars());
                    elemFaceVars.bindElement(element, fvGeometry, curSol);


                    for (auto&& scvf : scvfs(fvGeometry))
                    {
                        if (!couplingManager().isCoupledEntity(CouplingManager::stokesIdx, scvf))
                            continue;

                        // NOTE: binding the coupling context is necessary
                        couplingManager_->bindCouplingContext(CouplingManager::stokesIdx, element);
                        const auto flux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);

                        #if NONISOTHERMAL
                        const auto fluxEnergy = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf, diffCoeffAvgType_);

                        #endif

                        evaporation = flux[1] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()* FluidSystem::molarMass(1);

                        evaporationHDO = flux[2] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()* FluidSystem::molarMass(2);
                        evaporationH2O18= flux[3] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()* FluidSystem::molarMass(3);

                        #if NONISOTHERMAL
                        energyFlux = fluxEnergy * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                        #endif

                        evaporation += evaporationH2O18 + evaporationHDO;
                    }
                }
            }
            std::cout<<"evaporation in ff "<<evaporation<<std::endl;
            std::cout<<"energyFlux in ff "<<energyFlux<<std::endl;

        }

        /*!
         * \brief The problem name.
         */
        const std::string& name() const
        {
            return problemName_;
        }

        /*!
         * \name Problem parameters
         */
        // \{

        /*!
         * \brief Returns the temperature within the domain in [K].
         */
        Scalar temperature() const
        { return refTemperature_; }


        /*!
         * \brief Returns the sources within the domain.
         *
         * \param globalPos The global position
         */
        NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
        { return NumEqVector(0.0); }

        // \}
        /*!
         * \name Boundary conditions
         */
        // \{

        /*!
         * \brief Specifies which kind of boundary condition should be
         *        used for which equation on a given boundary segment.
         *
         * \param element The finite element
         * \param scvf The sub control volume face
         */
        BoundaryTypes boundaryTypes(const Element& element,
                                    const SubControlVolumeFace& scvf) const
                                    {


                                        BoundaryTypes values;

                                        const auto& globalPos = scvf.center();

                                        #if NONISOTHERMAL
                                        values.setNeumann(Indices::energyEqIdx);
                                        #endif

                                        if (onLeftBoundary_(globalPos))
                                        {
                                            values.setDirichlet(Indices::velocityXIdx);
                                            values.setDirichlet(Indices::velocityYIdx);
                                            values.setDirichlet(Indices::conti0EqIdx + 1);
                                            values.setDirichlet(Indices::conti0EqIdx + 2);
                                            values.setDirichlet(Indices::conti0EqIdx + 3);
                                            #if NONISOTHERMAL
                                            values.setDirichlet(Indices::temperatureIdx);
                                            #endif
                                            values.setDirichlet(Indices::turbulentKineticEnergyIdx);
                                            values.setDirichlet(Indices::dissipationIdx);
                                        }

                                        else if (onRightBoundary_(globalPos))
                                        {
                                            values.setDirichlet(Indices::pressureIdx);
                                            values.setOutflow(Indices::conti0EqIdx + 1);
                                            values.setOutflow(Indices::conti0EqIdx + 2);
                                            values.setOutflow(Indices::conti0EqIdx + 3);
                                            values.setOutflow(Indices::turbulentKineticEnergyEqIdx);
                                            values.setOutflow(Indices::dissipationEqIdx);

                                            #if NONISOTHERMAL
                                            values.setOutflow(Indices::energyEqIdx);
                                            #endif
                                        }

                                        else if (couplingManager().isCoupledEntity(CouplingManager::stokesIdx, scvf))
                                        {
                                            values.setCouplingNeumann(Indices::conti0EqIdx);
                                            values.setCouplingNeumann(Indices::conti0EqIdx + 1);
                                            values.setCouplingNeumann(Indices::conti0EqIdx + 2);
                                            values.setCouplingNeumann(Indices::conti0EqIdx + 3);
                                            values.setCouplingNeumann(Indices::momentumYBalanceIdx);
                                            values.setBeaversJoseph(Indices::momentumXBalanceIdx);
                                            #if NONISOTHERMAL
                                            values.setCouplingNeumann(Indices::energyEqIdx);
                                            #endif
                                        }

                                        else
                                        {
                                            values.setDirichlet(Indices::velocityXIdx);
                                            values.setDirichlet(Indices::velocityYIdx);
                                            values.setNeumann(Indices::conti0EqIdx);
                                            values.setNeumann(Indices::conti0EqIdx + 1);
                                            values.setNeumann(Indices::conti0EqIdx + 2);
                                            values.setNeumann(Indices::conti0EqIdx + 3);
                                            values.setDirichlet(Indices::turbulentKineticEnergyEqIdx);
                                            values.setDirichlet(Indices::dissipationEqIdx);
                                            #if NONISOTHERMAL
                                            values.setNeumann(Indices::energyEqIdx);
                                            #endif
                                        }

                                        return values;
                                    }
                                    /*!
                                     * \brief Evaluate the boundary conditions for fixed values at cell centers
                                     *
                                     * \param element The finite element
                                     * \param scv the sub control volume
                                     * \note used for cell-centered discretization schemes
                                     */
                                    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
                                    {
                                        const auto globalPos = scv.center();
                                        PrimaryVariables values(initialAtPos(globalPos));
                                        unsigned int  elementIdx = this->gridGeometry().elementMapper().index(element);

                                        static_assert(ModelTraits::turbulenceModel() == TurbulenceModel::komega, "Only valid for Komega");
                                        // For the komega model we set a fixed value for the dissipation
                                        const auto wallDistance = ParentType::wallDistance(elementIdx);
                                        using std::pow;
                                        values[Indices::dissipationEqIdx] = 6.0 * ParentType::kinematicViscosity(elementIdx)
                                        / (ParentType::betaOmega() * wallDistance * wallDistance);
                                        return values;

                                    }


                                    /*!
                                     * \brief Evaluates the boundary conditions for a Dirichlet control volume.
                                     *
                                     *
                                     */
                                    //     PrimaryVariables dirichletAtPos(const GlobalPosition& pos) const
                                    //     {
                                    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
                                    {
                                        const auto globalPos = scvf.ipGlobal();
                                        PrimaryVariables values(0.0);
                                        values = initialAtPos(globalPos);

                                        return values;
                                    }

                                    /*!
                                     * \brief Evaluates the boundary conditions for a Neumann control volume.
                                     *
                                     * \param element The element for which the Neumann boundary condition is set
                                     * \param fvGeometry The fvGeometry
                                     * \param elemVolVars The element volume variables
                                     * \param elemFaceVars The element face variables
                                     * \param scvf The boundary sub control volume face
                                     */
                                    NumEqVector neumann(const Element& element,
                                                        const FVElementGeometry& fvGeometry,
                                                        const ElementVolumeVariables& elemVolVars,
                                                        const ElementFaceVariables& elemFaceVars,
                                                        const SubControlVolumeFace& scvf) const
                                                        {
                                                            PrimaryVariables values(0.0);

                                                            if(couplingManager().isCoupledEntity(CouplingManager::stokesIdx, scvf))
                                                            {
                                                                values[Indices::momentumYBalanceIdx] = couplingManager().couplingData().momentumCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);

                                                                const auto massFlux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf, diffCoeffAvgType_);
                                                                values[Indices::conti0EqIdx] = massFlux[0];
                                                                values[Indices::conti0EqIdx + 1] = massFlux[1];
                                                                values[Indices::conti0EqIdx+2] = massFlux[2];
                                                                values[Indices::conti0EqIdx+3] = massFlux[3];

                                                                #if NONISOTHERMAL
                                                                values[Indices::energyEqIdx] = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf, diffCoeffAvgType_);
                                                                #endif

                                                            }
                                                            return values;
                                                        }

                                                        // \}
                                                        /*!
                                                         * \brief Set the coupling manager
                                                         */
                                                        void setCouplingManager(std::shared_ptr<CouplingManager> cm)
                                                        { couplingManager_ = cm; }


                                                        //! Get the coupling manager
                                                        const CouplingManager& couplingManager() const
                                                        { return *couplingManager_; }

                                                        /*!
                                                         * \name Volume terms
                                                         */
                                                        // \{

                                                        /*!
                                                         * \brief Evaluates the initial value for a control volume.
                                                         *
                                                         * \param globalPos The global position
                                                         */
                                                        PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
                                                        {
                                                            FluidState fluidState;
                                                            updateFluidStateForBC_(fluidState, refPressure());

                                                            const Scalar density = FluidSystem::density(fluidState, 0);

                                                            PrimaryVariables values(0.0);
                                                            values[Indices::pressureIdx] = refPressure() + density*this->gravity()[1]*(globalPos[1] - this->gridGeometry().bBoxMin()[1]);
                                                            values[Indices::conti0EqIdx + 1] = refMoleFrac();
                                                            values[Indices::conti0EqIdx + 2] = refMoleFracD();
                                                            values[Indices::conti0EqIdx + 3] = refMoleFracO18();
                                                            values[Indices::velocityXIdx] = refVelocity();

                                                            #if NONISOTHERMAL
                                                            values[Indices::temperatureIdx] = refTemperature();
                                                            #endif
                                                            //RANS
                                                            values[Indices::turbulentKineticEnergyEqIdx] = turbulentKineticEnergy_;
                                                            values[Indices::dissipationEqIdx] = dissipation_;
                                                            if (isOnWallAtPos(globalPos))
                                                            {
                                                                values[Indices::turbulentKineticEnergyEqIdx] = 0.0;
                                                                values[Indices::dissipationEqIdx] = 0.0;
                                                                values[Indices::velocityXIdx] = 0.0;
                                                            }

                                                            return values;
                                                        }

                                                        //! Returns the reference velocity.
                                                        const Scalar refVelocity() const
                                                        { return refVelocity_ ;}

                                                        //! Returns the reference pressure.
                                                        const Scalar refPressure() const
                                                        { return refPressure_; }

                                                        //! \brief Returns the mass fraction of H2O in the atmosphere
                                                        const Scalar refMoleFrac() const
                                                        { return relHumidity_*H2O::vaporPressure(refTemperature())/1e5; }

                                                        //! \brief Returns the mass fraction of HDO in the atmosphere
                                                        //The measured isotopic composition in the atmosphere refers to the vapour phase.
                                                        const Scalar refMoleFracD() const
                                                        { return (fracLiqD_*ref_D_/1000+ref_D_)*refMoleFrac(); }

                                                        //! \brief Returns the mass fraction of H2O18 in the atmosphere
                                                        //The measured isotopic composition in the atmosphere refers to the vapour phase.
                                                        const Scalar refMoleFracO18() const
                                                        { return (fracLiqO18_*ref_O18_/1000+ref_O18_)*refMoleFrac(); }

                                                        //! Returns the reference temperature.
                                                        const Scalar refTemperature() const
                                                        { return refTemperature_; }


                                                        void setTimeLoop(TimeLoopPtr timeLoop)
                                                        { timeLoop_ = timeLoop; }


                                                        void setTime(Scalar time)
                                                        { time_ = time; }

                                                        const Scalar time() const
                                                        {return time_; }

                                                        /*!
                                                         * \brief Returns the intrinsic permeability of required as input parameter
                                                         *        for the Beavers-Joseph-Saffman boundary condition.
                                                         */
                                                        Scalar permeability(const Element& element, const SubControlVolumeFace& scvf) const
                                                        {
                                                            return couplingManager().couplingData().darcyPermeability(element, scvf);
                                                        }

                                                        /*!
                                                         * \brief Returns the alpha value required as input parameter for the
                                                         *        Beavers-Joseph-Saffman boundary condition.
                                                         */
                                                        Scalar alphaBJ(const SubControlVolumeFace& scvf) const
                                                        {
                                                            return couplingManager().problem(CouplingManager::darcyIdx).spatialParams().beaversJosephCoeffAtPos(scvf.center());
                                                        }


                                                        bool isOnWall(const SubControlVolumeFace& scvf) const
                                                        {
                                                            GlobalPosition globalPos = scvf.ipGlobal();
                                                            return isOnWallAtPos(globalPos);
                                                        }

                                                        bool isOnWallAtPos(const GlobalPosition &globalPos) const
                                                        {
                                                            return ( onLowerBoundary_(globalPos));
                                                        }

                                                        //     \}

    private:
        bool onLeftBoundary_(const GlobalPosition &globalPos) const
        { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

        bool onRightBoundary_(const GlobalPosition &globalPos) const
        { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

        bool onLowerBoundary_(const GlobalPosition &globalPos) const
        { return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_; }

        bool onUpperBoundary_(const GlobalPosition &globalPos) const
        { return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_; }

        //! Updates the fluid state to obtain required quantities for IC/BC
        void updateFluidStateForBC_(FluidState& fluidState, const Scalar pressure) const
        {
            fluidState.setTemperature(refTemperature());
            fluidState.setPressure(0, pressure);
            fluidState.setSaturation(0, 1.0);
            fluidState.setMoleFraction(0, 1, refMoleFrac());
            fluidState.setMoleFraction(0, 0, 1.0 - refMoleFrac());

            typename FluidSystem::ParameterCache paramCache;
            paramCache.updatePhase(fluidState, 0);

            const Scalar density = FluidSystem::density(fluidState, paramCache, 0);
            fluidState.setDensity(0, density);

            const Scalar molarDensity = FluidSystem::molarDensity(fluidState, paramCache, 0);
            fluidState.setMolarDensity(0, molarDensity);

            const Scalar enthalpy = FluidSystem::enthalpy(fluidState, paramCache, 0);
            fluidState.setEnthalpy(0, enthalpy);
        }

        // the height of the free-flow domain
        const Scalar height_() const
        { return this->gridGeometry().bBoxMax()[1] - this->gridGeometry().bBoxMin()[1]; }

        Scalar eps_;

        Scalar refVelocity_;
        Scalar refPressure_;
        Scalar relHumidity_;
        Scalar refTemperature_;
        Scalar fracLiqD_;
        Scalar fracLiqO18_;
        Scalar ref_D_ = 155.76e-6;
        Scalar ref_O18_ = 2005.2e-6;
        std::string problemName_;
        TimeLoopPtr timeLoop_;
        Scalar turbulentKineticEnergy_;
        Scalar dissipation_;
        Scalar viscosityTilde_;
        Scalar time_;
        Scalar initializationTime_;
        std::string turbulenceModelName_;

        std::shared_ptr<CouplingManager> couplingManager_;

        DiffusionCoefficientAveragingType diffCoeffAvgType_;
    };
} // end namespace Dumux

#endif
