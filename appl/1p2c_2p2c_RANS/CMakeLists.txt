add_input_file_links()

dune_add_test(NAME test_stokes1p2cdarcy2p2c_isotope_RANS
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK)

dune_add_test(NAME test_stokes1p2cdarcy2p2c_ni_isotope_RANS
             SOURCES main.cc
            COMPILE_DEFINITIONS NONISOTHERMAL=1
            CMAKE_GUARD HAVE_UMFPACK)
dune_symlink_to_source_files(FILES params_nonisothermal.input)

#set(CMAKE_BUILD_TYPE Debug)
