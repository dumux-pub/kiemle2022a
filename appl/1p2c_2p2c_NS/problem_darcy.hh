// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A simple Darcy test problem (cell-centered finite volume method).
 */
#ifndef DUMUX_DARCY2P2C_SUBPROBLEM_HH
#define DUMUX_DARCY2P2C_SUBPROBLEM_HH

#include <dune/grid/yaspgrid.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/common/boundarytypes.hh>

#include <dumux/material/H2OAirIsotopes.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2pnc/model.hh>

#include "spatialparams.hh"
#include <dumux/volumevariables2pnc.hh>
#include <dumux/iofields.hh>

#include <dumux/io/gnuplotinterface.hh>

#include <cmath>
#include <iostream>


namespace Dumux {
template <class TypeTag>
class DarcySubProblem;

namespace Properties {
// Create new type tags
namespace TTag {
#if !NONISOTHERMAL
struct DarcyTwoPTwoC { using InheritsFrom = std::tuple<TwoPNC, CCTpfaModel>; };
#else
struct DarcyTwoPTwoC { using InheritsFrom = std::tuple<TwoPNCNI, CCTpfaModel>; };
#endif
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::DarcyTwoPTwoC> { using type = Dumux::DarcySubProblem<TypeTag>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DarcyTwoPTwoC> { using type = FluidSystems::H2OAirIsotopes<GetPropType<TypeTag, Properties::Scalar>>; };

//! Set the default formulation to pw-Sn: This can be over written in the problem.
template<class TypeTag>
struct Formulation<TypeTag, TTag::DarcyTwoPTwoC>
{ static constexpr auto value = TwoPFormulation::p1s0; };

//// The gas component balance (air) is replaced by the total mass balance
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::DarcyTwoPTwoC> { static constexpr int value = GetPropType<TypeTag, Properties::FluidSystem>::numComponents; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DarcyTwoPTwoC> { using type = Dune::YaspGrid<2, Dune::TensorProductCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2> >; };

template<class TypeTag>
struct UseMoles<TypeTag, TTag::DarcyTwoPTwoC> { static constexpr bool value = true; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::DarcyTwoPTwoC>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = TwoPNCSpatialParams<GridGeometry, Scalar>;
};

template<class TypeTag>
struct IOFields<TypeTag, TTag::DarcyTwoPTwoC> { using type = TwoPNCIsotopeIOFields; };

template<class TypeTag>
struct SetMoleFractionsForFirstPhase<TypeTag, TTag::DarcyTwoPTwoC> { static constexpr bool value = true; };

#if !NONISOTHERMAL
//! Set the volume variables property
template<class TypeTag>
struct VolumeVariables<TypeTag, TTag::DarcyTwoPTwoC>
{
private:
    using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FSY = GetPropType<TypeTag, Properties::FluidSystem>;
    using FST = GetPropType<TypeTag, Properties::FluidState>;
    using SSY = GetPropType<TypeTag, Properties::SolidSystem>;
    using SST = GetPropType<TypeTag, Properties::SolidState>;
    using PT = typename GetPropType<TypeTag, Properties::SpatialParams>::PermeabilityType;
    using MT = GetPropType<TypeTag, Properties::ModelTraits>;
    static constexpr auto DM = GetPropType<TypeTag, Properties::GridGeometry>::discMethod;
    static constexpr bool enableIS = getPropValue<TypeTag, Properties::EnableBoxInterfaceSolver>();
    // class used for scv-wise reconstruction of non-wetting phase saturations
    using SR = TwoPScvSaturationReconstruction<DM, enableIS>;
    using BaseTraits = TwoPVolumeVariablesTraits<PV, FSY, FST, SSY, SST, PT, MT, SR>;

    using DT = GetPropType<TypeTag, Properties::MolecularDiffusionType>;
    using EDM = GetPropType<TypeTag, Properties::EffectiveDiffusivityModel>;
    template<class BaseTraits, class DT, class EDM>
    struct NCTraits : public BaseTraits
    {
        using DiffusionType = DT;
        using EffectiveDiffusivityModel = EDM;
    };
public:
    using type = TwoPNCVolumeVariablesIsotope<NCTraits<BaseTraits, DT, EDM>>;
};
#else
//! Set the volume variables property
template<class TypeTag>
struct VolumeVariables<TypeTag, TTag::DarcyTwoPTwoC>
{
private:
    using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FSY = GetPropType<TypeTag, Properties::FluidSystem>;
    using FST = GetPropType<TypeTag, Properties::FluidState>;
    using SSY = GetPropType<TypeTag, Properties::SolidSystem>;
    using SST = GetPropType<TypeTag, Properties::SolidState>;
    using PT = typename GetPropType<TypeTag, Properties::SpatialParams>::PermeabilityType;
    using MT = GetPropType<TypeTag, Properties::ModelTraits>;
    static constexpr auto DM = GetPropType<TypeTag, Properties::GridGeometry>::discMethod;
    static constexpr bool enableIS = getPropValue<TypeTag, Properties::EnableBoxInterfaceSolver>();
    // class used for scv-wise reconstruction of non-wetting phase saturations
    using SR = TwoPScvSaturationReconstruction<DM, enableIS>;
    using BaseTraits = TwoPVolumeVariablesTraits<PV, FSY, FST, SSY, SST, PT, MT, SR>;

    using DT = GetPropType<TypeTag, Properties::MolecularDiffusionType>;
    using EDM = GetPropType<TypeTag, Properties::EffectiveDiffusivityModel>;
    using ETCM = GetPropType< TypeTag, Properties:: ThermalConductivityModel>;
    template<class BaseTraits, class DT, class EDM, class ETCM>
    struct NCNITraits : public BaseTraits
    {
        using DiffusionType = DT;
        using EffectiveDiffusivityModel = EDM;
        using EffectiveThermalConductivityModel = ETCM;
    };
public:
    using type = TwoPNCVolumeVariablesIsotope<NCNITraits<BaseTraits, DT, EDM, ETCM>>;
};
#endif

} // end namespace Properties

template <class TypeTag>
class DarcySubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;

    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;

    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum {
        // primary variable indices
        conti0EqIdx = Indices::conti0EqIdx,
        contiWEqIdx = Indices::conti0EqIdx + FluidSystem::H2OIdx,
        contiNEqIdx = Indices::conti0EqIdx + FluidSystem::AirIdx,
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx
    };

    #if NONISOTHERMAL
    enum {energyEqIdx = Indices::energyEqIdx}; //water temp
    #endif

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    using DiffusionCoefficientAveragingType = typename StokesDarcyCouplingOptions::DiffusionCoefficientAveragingType;

    using H2O = Dumux::Components::H2O<Scalar>;
    using Air = Dumux::Components::Air<Scalar>;
    using HDO = Dumux::Components::HDO<Scalar>;
    using H2O18 = Dumux::Components::H2O18<Scalar>;

public:
    DarcySubProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(gridGeometry, "Darcy"), eps_(1e-7), couplingManager_(couplingManager)
    {
        pressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Pressure");
        initialSw_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Saturation");
        temperature_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Temperature");
        initialPhasePresence_ = getParamFromGroup<int>(this->paramGroup(), "Problem.InitPhasePresence");
        delta_D_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.DeltaD");
        delta_O18_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.DeltaO18");
        xw_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Xw");


        diffCoeffAvgType_ = StokesDarcyCouplingOptions::stringToEnum(DiffusionCoefficientAveragingType{}, getParamFromGroup<std::string>(this->paramGroup(), "Problem.InterfaceDiffusionCoefficientAvg"));
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        outputname_ = getParam<std::string>("Problem.Name");
        initializationTime_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitializationTime");


    }

    void setTime(Scalar time)
    { time_ = time; }

    const Scalar time() const
    {return time_; }

    void setPreviousTime(Scalar time)
    { previousTime_ = time; }

    const Scalar previousTime() const
    {return previousTime_; }




    /*!
        * \brief The problem name.
        */
    const std::string& name() const
    {
        return problemName_;
    }

    template<class SolutionVector, class GridVariables>
    void printWaterMass(const SolutionVector& curSol,
                        const GridVariables& gridVariables,
                        const Scalar timeStepSize)

    {

        // compute the mass in the entire domain
        Scalar evaporation = 0.0;
        Scalar evaporationHDO = 0.0;
        Scalar evaporationH2O18 = 0.0;
        Scalar energyFlux = 0.0;
#if NONISOTHERMAL
        Scalar averageTemperature = 0.0;
        Scalar numScvf = 0.0;
#endif

            if (!(time() < 0.0))
        {
        // bulk elements
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, curSol);

            for (auto&& scvf : scvfs(fvGeometry))
            {
                if (!couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
                    continue;

                // NOTE: binding the coupling context is necessary
                couplingManager_->bindCouplingContext(CouplingManager::darcyIdx, element);
                const auto flux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf);
                #if NONISOTHERMAL
                const auto fluxEnergy = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, scvf,diffCoeffAvgType_);
                #endif

                evaporation = flux[0] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()* FluidSystem::molarMass(FluidSystem::H2OIdx);
                evaporationHDO = flux[2] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()* FluidSystem::molarMass(FluidSystem::HDOIdx);
                evaporationH2O18 = flux[3] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()* FluidSystem::molarMass(FluidSystem::H2O18Idx);
                #if NONISOTHERMAL
                energyFlux = fluxEnergy* scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                averageTemperature += elemVolVars[scvf.insideScvIdx()].temperature();
                numScvf += 1;
                #endif

                evaporation += evaporationH2O18 + evaporationHDO;

        }

            #if NONISOTHERMAL
            averageTemperature = averageTemperature/numScvf;

            #endif
        }
        }

        std::cout <<"evaporation from pm "<<evaporation<<std::endl;
        std::cout <<"energyFlux from pm "<<energyFlux<<std::endl;

        //do a gnuplot
        x_.push_back(time()/3600); // in hours
        y_.push_back(evaporation);
        y2_.push_back(evaporationHDO);
        y3_.push_back(evaporationH2O18);

        gnuplot_.resetPlot();
        gnuplot_.setXRange(0,std::max(time()/3600, 1.0));
        gnuplot_.setYRange(0.0, 5e-5);
        gnuplot_.setXlabel("time [h]");
        gnuplot_.setYlabel("kg/s");
        #if NONISOTHERMAL
        gnuplot_.addDataSetToPlot(x_, y_, "evaporation_rate_" + outputname_);
        gnuplot_.addDataSetToPlot(x_, y2_, "evaporation_rate_HDO_" + outputname_);
        gnuplot_.addDataSetToPlot(x_, y3_, "evaporation_rate_H2O18_" + outputname_);

        #else
        gnuplot_.addDataSetToPlot(x_, y_, "evaporation_rate_isothermal_" + outputname_);
        gnuplot_.addDataSetToPlot(x_, y2_, "evaporation_rate_isothermal_HDO_" + outputname_);
        gnuplot_.addDataSetToPlot(x_, y3_, "evaporation_rate_isothermal_H2O18_" + outputname_);
        #endif
        gnuplot_.plot("evaporation");

    }

    /*!
        * \name Problem parameters
        */
    // \{

    /*!
        * \brief Returns the temperature within the domain in [K].
        */
    Scalar temperature() const
    { return temperature_; }
    // \}

    //! \brief Returns the mole fraction of HDO in the gas phase in soil
    const Scalar refMoleFracD() const
    { return (delta_D_*ref_D_/1000+ref_D_)*xw_; }

    //! \brief Returns the mole fraction of H2O18 in the gas phase in soil
    const Scalar refMoleFracO18() const
    { return (delta_O18_*ref_O18_/1000+ref_O18_)*xw_; }

/*!
    * \name Boundary conditions
    */
// \{
/*!
    * \brief Specifies which kind of boundary condition should be
    *        used for which equation on a given boundary control volume.
    *
    * \param element The element
    * \param scvf The boundary sub control volume face
    */
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolumeFace &scvf) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
            values.setAllCouplingNeumann();

        return values;
    }

    /*!
        * \brief Evaluates the boundary conditions for a Dirichlet control volume.
        *
        * \param element The element for which the Dirichlet boundary condition is set
        * \param scvf The boundary sub control volume face
        *
        * For this method, the \a values parameter stores primary variables.
        */
    PrimaryVariables dirichlet(const Element &element, const SubControlVolumeFace &scvf) const
    {
        PrimaryVariables values(0.0);
        values = initialAtPos(scvf.center());

        return values;
    }

    /*!
        * \brief Evaluates the boundary conditions for a Neumann control volume.
        *
        * \param element The element for which the Neumann boundary condition is set
        * \param fvGeometry The fvGeometry
        * \param elemVolVars The element volume variables
        * \param elemFluxVarsCache Flux variables caches for all faces in stencil
        * \param scvf The boundary sub control volume face
        */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
# if !NONISOTHERMAL
        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
        {

            values = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_);
        }
#else
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];
        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
        {
            const auto massFlux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_);
            for(int i = 0; i< massFlux.size(); ++i)
                values[i] = massFlux[i];

            values[Indices::energyEqIdx] = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_);
        }
        else
        {
            Scalar temperatureInside = volVars.temperature();
            Scalar plexiglassThermalConductivity =
            getParamFromGroup<Scalar>(this->paramGroup(), "Problem.PlexiglassThermalConductivity");
            Scalar plexiglassThickness =  getParamFromGroup<Scalar>(this->paramGroup(), "Problem.PlexiglassThickness");
            values[energyEqIdx] = plexiglassThermalConductivity
            * (temperatureInside - temperature_)
            / plexiglassThickness;

        }
#endif
        return values;
    }

// \}

    /*!
    * \name Volume terms
    */
    // \{
    /*!
    * \brief Evaluates the source term for all phases within a given
    *        sub control volume.
    *
    * \param element The element for which the source term is set
    * \param fvGeometry The fvGeometry
    * \param elemVolVars The element volume variables
    * \param scv The sub control volume
    *
    * For this method, the \a values variable stores the rate mass
    * of a component is generated or annihilated per volume
    * unit. Positive values mean that mass is created, negative ones
    * mean that it vanishes.
    */
    NumEqVector source(const Element &element,
                    const FVElementGeometry& fvGeometry,
                    const ElementVolumeVariables& elemVolVars,
                    const SubControlVolume &scv) const
                    { return NumEqVector(0.0); }

                // \}

    /*!
    * \brief Evaluates the initial value for a control volume.
    *
    * For this method, the \a priVars parameter stores primary
    * variables.
    */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        values.setState(initialPhasePresence_);
        values[pressureIdx] = pressure_;
        values[switchIdx] = initialSw_;
        values[Indices::conti0EqIdx+2] =refMoleFracD();
        values[Indices::conti0EqIdx+3] = refMoleFracO18();

        #if NONISOTHERMAL
        values[Indices::temperatureIdx] = temperature_;
        #endif
        return values;
    }

    // \}

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_; }

    Scalar pressure_;
    Scalar initialSw_;
    Scalar temperature_;
    Scalar delta_D_;
    Scalar delta_O18_;
    int initialPhasePresence_;
    std::string problemName_;
    std::string outputname_ ;
    Scalar eps_;
    Scalar initializationTime_;
    Scalar time_;
    Scalar previousTime_;
    Scalar xw_;

    Scalar ref_D_ = 155.76e-6;
    Scalar ref_O18_ = 2005.2e-6;

    std::shared_ptr<CouplingManager> couplingManager_;
    DiffusionCoefficientAveragingType diffCoeffAvgType_;

    Dumux::GnuplotInterface<double> gnuplot_;


    std::vector<double> x_;
    std::vector<double> y_;
    std::vector<double> y2_;
    std::vector<double> y3_;
    std::vector<double> y4_;
    std::vector<double> y5_;
    std::vector<double> y6_;
    std::vector<double> y7_;
    std::vector<double> y8_;
    std::vector<double> y9_;
    std::vector<double> y10_;
};
} // end namespace Dumux

#endif
