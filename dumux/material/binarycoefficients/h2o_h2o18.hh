// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Binarycoefficients
 * \brief Binary coefficients for water and H2O18.
 */
#ifndef DUMUX_BINARY_COEFF_H2O_H2O18_HH
#define DUMUX_BINARY_COEFF_H2O_H2O18_HH

#include <cmath>

namespace Dumux {
    namespace BinaryCoeff {

        /*!
         * \ingroup Binarycoefficients
         * \brief Binary coefficients for water and H2O18.
         */
        class H2O_H2O18
        {
        public:
            /*!
             * \brief Henry coefficient \f$\mathrm{[Pa]}\f$  for isotope in liquid water.
             * \param temperature the temperature \f$\mathrm{[K]}\f$
             */

            template <class Scalar>
            static Scalar henry(Scalar temperature)
            {

                DUNE_THROW(Dune::NotImplemented,
                           "H2O_H2O18Binarycoefficient::henry()");

            }

            /*!
             * \brief Binary diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular water and air; Lacking
             * data on isotope-water diffusion in gases, we use at the moment the diffusion coeffcient of water in air.
             *
             * \param temperature the temperature \f$\mathrm{[K]}\f$
             * \param pressure the phase pressure \f$\mathrm{[Pa]}\f$
             * Vargaftik: Tables on the thermophysical properties of liquids and gases.
             * John Wiley & Sons, New York, 1975. \cite vargaftik1975 <BR>
             * Walker, Sabey, Hampton: Studies of heat transfer and water migration in soils.
             * Dep. of Agricultural and Chemical Engineering, Colorado State University,
             * Fort Collins, 1981. \cite walker1981
             */
            template <class Scalar>
            static Scalar gasDiffCoeff(Scalar temperature, Scalar pressure)
            {
                        const Scalar Theta=1.8;
                        const Scalar Daw=2.13e-5;  /* reference value */
                        const Scalar pg0=1.e5;     /* reference pressure */
                        const Scalar T0=273.15;    /* reference temperature */
                        Scalar Dgaw;

                        using std::pow;
                        Dgaw=Daw*(pg0/pressure)*pow((temperature/T0),Theta);
                        return Dgaw;
            }

            /*!
             * Lacking better data on water-air diffusion in liquids, we use at the
             * moment the diffusion coefficient of the air's main component nitrogen!!
             * \brief Diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular nitrogen in liquid isotopic water.
             *
             * \param temperature the temperature \f$\mathrm{[K]}\f$
             * \param pressure the phase pressure \f$\mathrm{[Pa]}\f$
             *
             */
            template <class Scalar>
            static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
            {

                using std::pow;
                using std::exp;
                const Scalar a = 0.9669;
                Scalar Dliq = a*pow(10,-9)*exp((-535400/pow(temperature,2))+(1393.3/temperature)+ 2.1876);
                return Dliq;
            }
        };

    } // end namespace BinaryCoeff
} // end namespace Dumux

#endif
