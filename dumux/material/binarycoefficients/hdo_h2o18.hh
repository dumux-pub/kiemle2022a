// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Binarycoefficients
 * \brief Binary coefficients for heavy water and air.
 */
#ifndef DUMUX_BINARY_COEFF_HDO_H2O18_HH
#define DUMUX_BINARY_COEFF_HDO_H2O18_HH

#include <cmath>
#include <dumux/material/binarycoefficients/fullermethod.hh>
#include <dumux/material/components/hdo.hh>
#include <dumux/material/components/h2o18.hh>
#include <dumux/material/components/air.hh>

namespace Dumux {
    namespace BinaryCoeff {

        /*!
         * \ingroup Binarycoefficients
         * \brief Binary coefficients for HDO and H2O18.
         */
        class HDO_H2O18
        {
        public:
            /*!
             * \brief Henry coefficient \f$\mathrm{[Pa]}\f$  for air in liquid water.
             * \param temperature the temperature \f$\mathrm{[K]}\f$
             */
            template <class Scalar>
            static Scalar henry(Scalar temperature)
            {

                     DUNE_THROW(Dune::NotImplemented,
                           "HDO_H2O18Binarycoefficient::henry()");
            }

            /*!
             * \brief Binary diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular water and air
             *
             * \param temperature the temperature \f$\mathrm{[K]}\f$
             * \param pressure the phase pressure \f$\mathrm{[Pa]}\f$

             */
            template <class Scalar>
            static Scalar gasDiffCoeff(Scalar temperature, Scalar pressure)
            {

                DUNE_THROW(Dune::NotImplemented,"HDO_H2O18Binarycoefficient::gasDiffCoeff()");
            }

            /*!
             * Lacking better data on water-air diffusion in liquids, we use at the
             * moment the diffusion coefficient of the air's main component nitrogen!!
             * \brief Diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular nitrogen in liquid water.

             */
            template <class Scalar>
            static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
            {
                DUNE_THROW(Dune::NotImplemented,"HDO_H2O18Binarycoefficient::liquidDiffCoeff()");
            }
        };

    } // end namespace BinaryCoeff
} // end namespace Dumux

#endif
