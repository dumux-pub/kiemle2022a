// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief Properties of H2O18.
 */
#ifndef DUMUX_H2O18_HH
#define DUMUX_H2O18_HH

#include <cmath>
#include <dumux/material/idealgas.hh>
#include <dumux/material/constants.hh>

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>
#include <dumux/material/components/gas.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief Properties of H2O18. Commented if values from water are used instead of H2O18 values
 *
 * \tparam Scalar The type used for scalar values
 */
template <class Scalar>
class H2O18
: public Components::Base<Scalar, H2O18<Scalar> >
, public Components::Liquid<Scalar, H2O18<Scalar> >
, public Components::Gas<Scalar, H2O18<Scalar> >
{
    using Consts = Constants<Scalar>;
    using IdealGas = Dumux::IdealGas<Scalar>;

public:
    /*!
     * \brief A human readable name for the H2O18
     */
    static std::string name()
    { return "H2O18"; }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of H2O18
     */
    constexpr static Scalar molarMass() //für HD16O
    { return 0.020013; }

    /*!
     * \brief Returns the critical temperature \f$\mathrm{[K]}\f$ of H2O18
     */
    constexpr static Scalar criticalTemperature()
    { return 647.1; } //Quelle: IAPWS G5-01(2016); kritische Temperatur von Wasser

    /*!
     * \brief Returns the critical pressure \f$\mathrm{[Pa]}\f$ of H2O18
     */
    constexpr static Scalar criticalPressure()
    { return 22.064e5; } //Quelle: IAPWS G5-01(2016); kritischer Druck von Wasser

    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ at H2O18's boiling point (1 atm).
     */
    constexpr static Scalar boilingTemperature()
    { return 373.29; }
    //Quelle: stable isotope chemistry (book)
    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ at H2O18's triple point.
     */
    static Scalar tripleTemperature()
    {return 273.46; }
    //Quelle: V. F. Petrenko and R. W. Whitworth, Physics of ice (Oxford University Press, Oxford, 1999).

    /*!
     * \brief Returns the pressure \f$\mathrm{[Pa]}\f$ at H2O18's triple point.
     */
    static Scalar triplePressure()
    {
        //DUNE_THROW(Dune::NotImplemented, "triplePressure for H2O18");
        return 611.65; // Druck am Triplepunkt für Wasser; Quelle: IAPWS G12-15
    }

    /*!
     * \brief The saturation vapor pressure in \f$\mathrm{[Pa]}\f$ of pure H2O
     *        at a given temperature according to Antoine after Betz 1997 ->  Gmehling et al 1980 \cite gmehling1980 <BR>
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     */
    static Scalar vaporPressure(Scalar temperature) //Parameter für Antoines Gl. für Wasser Quelle: Foliensatz Multiphase Modelling
    {
//         const Scalar A = 8.07131;
//         const Scalar B = 1730.63;
//         const Scalar C = 233.426;
//
//         Scalar T = temperature - 273.15;
//
//         using std::pow;
//         Scalar psatH2O= 1.334*pow(10.0, (A - (B/(T + C))));  // in [mbar]
//         psatH2O *= 100.0;  // in [Pa] (0.001*1.E5)
//

         Scalar T = temperature ;
                if (T > criticalTemperature())
            return criticalPressure();
        if (T < tripleTemperature())
            return 0; // water is solid: We don't take sublimation into account

        static const Scalar n[10] = {
            0.11670521452767e4, -0.72421316703206e6, -0.17073846940092e2,
            0.12020824702470e5, -0.32325550322333e7, 0.14915108613530e2,
            -0.48232657361591e4, 0.40511340542057e6, -0.23855557567849,
            0.65017534844798e3
        };

        Scalar sigma = T + n[8]/(T - n[9]);

        Scalar A = (sigma + n[0])*sigma + n[1];
        Scalar B = (n[2]*sigma + n[3])*sigma + n[4];
        Scalar C = (n[5]*sigma + n[6])*sigma + n[7];

        using std::sqrt;
        Scalar tmp = Scalar(2.0)*C/(sqrt(B*B - Scalar(4.0)*A*C) - B);
        tmp *= tmp;
        tmp *= tmp;

        Scalar pH2O =  Scalar(1e6)*tmp;


        // Parameter for vapor pressure H2O18 and equation: Reference: van Hook (1967)
        const Scalar D = 1991.1; // entspricht A in Quelle
        const Scalar E = -4.1887; // entspricht B in Quelle
        const Scalar F = 0.001197; // entspricht C in Quelle

        using std::exp;
        //Scalar psat = psatH2O*exp(-((D/pow(temperature,2))+(E/temperature)+F));
         Scalar psat = pH2O*exp(-((D/pow(temperature,2))+(E/temperature)+F));
        return psat;
    }

    /*!
     * \brief Specific heat cap of liquid H2O18 \f$\mathrm{[J/kg]}\f$.
     *
     * source : Reid et al. (fourth edition): Missenard group contrib. method (chap 5-7, Table 5-11, s. example 5-8) \cite reid1987 <BR>
     *
     * \param temp temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidHeatCapacity(Scalar temp, Scalar pressure)
    {
        Scalar H,O;
        // after Reid et al. : Missenard group contrib. method (s. example 5-8) \cite reid1987 <BR>
        // Für Wasser
        // linear interpolation between table values [J/(mol K)]

        if(temp < 298.0){                              // take care: extrapolation for Temp<273
            H = 13.4 + 1.2*(temp - 273.0)/25.0;        // 13.4 + 1.2 = 14.6 = H(T=298K) i.e. interpolation of table values 273<T<298
            O = 29.3 + 0.4*(temp - 273.0)/25.0;

        }
        else if(temp < 323.0){
            H = 14.6 + 0.9*(temp - 298.0)/25.0;        // i.e. interpolation of table values 298<T<323
            O = 29.7 + 0.4*(temp - 298.0)/25.0;

        }
        else if(temp < 348.0){
            H = 15.5 + 1.2*(temp - 323.0)/25.0;        // i.e. interpolation of table values 323<T<348
            O = 30.1 + 0.4*(temp - 323.0)/25.0;

        }
        else {
            H = 16.7 + 2.1*(temp - 348.0)/25.0;         // i.e. interpolation of table values 348<T<373
            O = 30.5 + 0.5*(temp - 348.0)/25.0;        // take care: extrapolation for Temp>373
        }

        return (O + 2*H)/molarMass();// J/(mol K) -> J/(kg K)
    }


    /*!
     * \brief Specific enthalpy of liquid H2O18 \f$\mathrm{[J/kg]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidEnthalpy(const Scalar temperature,
                                 const Scalar pressure)
    {
        // Gauss quadrature rule:
        // Interval: [0K; temperature (K)]
        // Gauss-Legendre-Integration with variable transformation:
        // \int_a^b f(T) dT  \approx (b-a)/2 \sum_i=1^n \alpha_i f( (b-a)/2 x_i + (a+b)/2 )
        // with: n=2, legendre -> x_i = +/- \sqrt(1/3), \apha_i=1
        // here: a=273.15K, b=actual temperature in Kelvin
        // \leadsto h(T) = \int_273.15^T c_p(T) dT
        //              \approx 0.5 (T-273.15) * (cp( 0.5(temperature-273.15)sqrt(1/3) ) + cp(0.5(temperature-273.15)(-1)sqrt(1/3))

        // Enthalpy may have arbitrary reference state, but the empirical/fitted heatCapacity function needs Kelvin as input and is
        // fit over a certain temperature range. This suggests choosing an interval of integration being in the actual fit range.
        // I.e. choosing T=273.15K  as reference point for liquid enthalpy.
        using std::sqrt;
        const Scalar sqrt1over3 = sqrt(1./3.);
        // evaluation points according to Gauss-Legendre integration
        const Scalar TEval1 = 0.5*(temperature-273.15)*        sqrt1over3 + 0.5*(273.15+temperature);
        // evaluation points according to Gauss-Legendre integration
        const Scalar TEval2 = 0.5*(temperature-273.15)* (-1)*  sqrt1over3 + 0.5*(273.15+temperature);

        const Scalar h_n = 0.5 * (temperature-273.15) * ( liquidHeatCapacity(TEval1, pressure) + liquidHeatCapacity(TEval2, pressure) );

        return h_n;
    }

    /*!
     * \brief Latent heat of vaporization for H2O18 \f$\mathrm{[J/kg]}\f$.
     *
     * source : Reid et al. (fourth edition): Chen method (chap. 7-11, Delta H_v = Delta H_v (T) according to chap. 7-12) \cite reid1987
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar heatVap(Scalar temperature,
                          const Scalar pressure)
    {
        using std::min;
        using std::max;
        temperature = min(temperature, criticalTemperature()); // regularization
        temperature = max(temperature, 0.0); // regularization

        constexpr Scalar T_crit = criticalTemperature();
        constexpr Scalar Tr1 = boilingTemperature()/criticalTemperature();
        constexpr Scalar p_crit = criticalPressure();

        // Chen method, eq. 7-11.4 (at boiling)
        using std::log;
        const Scalar DH_v_boil = Consts::R * T_crit * Tr1
                                  * (3.978 * Tr1 - 3.958 + 1.555*log(p_crit * 1e-5 /*Pa->bar*/ ) )
                                  / (1.07 - Tr1); /* [J/mol] */

        /* Variation with temp according to Watson relation eq 7-12.1*/
        using std::pow;
        const Scalar Tr2 = temperature/criticalTemperature();
        const Scalar n = 0.375;
        const Scalar DH_vap = DH_v_boil * pow(((1.0 - Tr2)/(1.0 - Tr1)), n);

        return (DH_vap/molarMass());          // we need [J/kg]
    }

    /*!
     * \brief Specific enthalpy of H2O18 vapor \f$\mathrm{[J/kg]}\f$.
     *
     *          This relation is true on the vapor pressure curve, i.e. as long
     *          as there is a liquid phase present.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar gasEnthalpy(Scalar temperature, Scalar pressure)
    {
        return liquidEnthalpy(temperature, pressure) + heatVap(temperature, pressure);
    }

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of H2O18 gas at a given pressure and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar gasDensity(Scalar temperature, Scalar pressure)
    {
        return IdealGas::density(molarMass(),
                                 temperature,
                                 pressure);
    }

    /*!
     * \brief The molar gas density \f$\mathrm{[mol/m^3]}\f$ of H2O18 gas at a given pressure and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar gasMolarDensity(Scalar temperature, Scalar pressure)
    { return IdealGas::molarDensity(temperature, pressure); }

    /*!
     * \brief The molar liquid density of pure H2O18 at a given pressure and temperature
     * \f$\mathrm{[mol/m^3]}\f$.
     *
     * source : Reid et al. (fourth edition): Modified Racket technique (chap. 3-11, eq. 3-11.9) \cite reid1987 <BR>
     *
     * \param temp temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidMolarDensity(Scalar temp, Scalar pressure)
    {
        // saturated molar volume according to Lide, CRC Handbook of
        // Thermophysical and Thermochemical Data, CRC Press, 1994
        // valid for 245 < Temp < 600
        using std::min;
        using std::max;
        temp = min(temp, 500.0); // regularization
        temp = max(temp, 250.0); // regularization

        // für Wasser; Quelle: Reid et al.
        using std::pow;
        const Scalar A1 = 0.02338;           // from table
        const Scalar A2 = 8.31448 *criticalTemperature()/criticalPressure();
        const Scalar expo = 1.0 + pow((1.0 - temp/criticalTemperature()), (2.0/7.0));
        const Scalar V = A2*pow(A1, expo);   // liquid molar volume [m^3/mol]

        return 1.0/V;             // molar density [mol/m^3]



       // return liquidMolarDensity(temperature, pressure)*molarMass(); // Alternative zur Berechnung über Parameter

    }

    /*!
     * \brief The density of pure H2O18 at a given pressure and temperature \f$\mathrm{[kg/m^3]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidDensity(Scalar temperature, Scalar pressure)
    {
        return liquidMolarDensity(temperature, pressure)*molarMass();
    }

    /*!
     * \brief Returns true if the gas phase is assumed to be compressible
     */
    static constexpr bool gasIsCompressible()
    { return true; }

    /*!
     * \brief Returns true if the gas phase is assumed to be ideal
     */
    static constexpr bool gasIsIdeal()
    { return true; }

    /*!
     * \brief Returns true if the liquid phase is assumed to be compressible
     */
    static constexpr bool liquidIsCompressible()
    { return false; }

    /*!
     * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of H2O18 vapor
     *
     * \param temp temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     * \param regularize defines, if the functions is regularized or not, set to true by default
     */
    static Scalar gasViscosity(Scalar temp, Scalar pressure, bool regularize=true)
    {
        using std::min;
        using std::max;
        temp = min(temp, 500.0); // regularization
        temp = max(temp, 250.0); // regularization

        using std::pow;
        using std::exp;
        const Scalar Tr = max(temp/criticalTemperature(), 1e-10);
        const Scalar Fp0 = 1.0;
        const Scalar xi = 0.176*pow((criticalTemperature()/pow((molarMass()/1000),3.0)/pow(criticalPressure(),4.0)),(1/6)); //Quelle: Reid et al. (1987) (9-4.14)
        const Scalar eta_xi = Fp0*(0.807*pow(Tr, 0.618)
                                   - 0.357*exp(-0.449*Tr)
                                   + 0.34*exp(-4.058*Tr)
                                   + 0.018);
        Scalar r = eta_xi/xi; // [1e-6 P]
        r /= 1.0e7; // [Pa s]

        return r;


        // Alternative:
        //return 1.056 //Viskosität bei T = 20°C; Quelle: stable isotope geochemistry (book)
    }

    /*!
     * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of pure H2O18.
     *
     * \param temp temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidViscosity(Scalar temp, Scalar pressure)
    {
        //Werte für Wasser Quelle: Reid et al. (1987)(Table 9-8)
        using std::min;
        using std::max;
        temp = min(temp, 500.0); // regularization
        temp = max(temp, 250.0); // regularization

        const Scalar A = -20.471;
        const Scalar B = 4.209e3;
        const Scalar C = 4.527e-2;
        const Scalar D = -3.376e-05;


        using std::exp;
        Scalar r = exp(A + B/temp + C*temp + D*temp*temp); // in [cP]
        r *= 1.0e-3; // in [Pa s]

        return r; // [Pa s]
    }

    /*!
     * \brief Thermal conductivity \f$\mathrm{[[W/(m*K)]}\f$ of H2O18
     *
     *
     * \param temperature absolute temperature in \f$\mathrm{[K]}\f$
     * \param pressure of the phase in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidThermalConductivity( Scalar temperature,  Scalar pressure)
    {
        return 0.61; //Für Wasser; Quelle IAPWS für 25°C bei 0.1MPa

        //Alternativ: IAPWS für Wasser bei 0,1MPa Quelle: IAPWS SR6-08(2011)
//         using std::pow;
//         Scalar Ts = temperature/300;
//         Scalar lambda = 1.663*pow(Ts,-1.15) -1.7781*pow(Ts,-3.4)+ 1.1567*pow(Ts,-6.0) -0.432115*pow(Ts,-7.6);
//         return lambda;

    }

    static Scalar gasThermalConductivity( Scalar temperature,  Scalar pressure)
    {
        return 0.025; //Für Wasser. Quelle:  http://webbook.nist.gov/cgi/fluid.cgi?ID=C7732185&Action=Page @ T= 372.76K (99.6°C) , p=0.1MPa)
    }

        static Scalar gasHeatCapacity(Scalar temperature, Scalar pressure)
    {
        return 2.08e3; //Für Water. Quelle: \dumux\component\simpleh2o
    }

};

} // end namespace Components

} // end namespace Dumux

#endif
