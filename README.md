## Content

This module contains all necessary code to reproduce the results in

S.Kiemle, K. Heck, E. Coltman, R. Helmig: Stable water isotopologue fractionation during
soil-water evaporation: Analysis using a coupled soil-atmosphere model

You can configure the module just like any other DUNE
module by using `dunecontrol`.


## Version Information

|      module name      |      branch name      |                 commit sha                 |         commit date         |
|-----------------------|-----------------------|--------------------------------------------|-----------------------------|
|         dumux         |  origin/releases/3.4  |  f78393cff93e246d736819d9f6efc261ef733a1f  |  2022-01-12 10:33:18 +0000  |
|      dune-common      |  origin/releases/2.8  |  fb179f9420efb44ba76bd6219823e436ef63122a  |  2021-09-24 05:38:46 +0000  |
|     dune-geometry     |  origin/releases/2.8  |  e7bfb66e48496aa28e47974c33ea9a4579bf723b  |  2021-08-31 17:51:20 +0000  |
|       dune-grid       |  origin/releases/2.8  |  e3371946f18df31d4ad542e5e7b51f652954edbc  |  2021-10-26 11:13:47 +0000  |
|       dune-istl       |  origin/releases/2.8  |  fffb544a61d2c65a0d2fc7c751f36909f06be8f5  |  2021-08-31 13:58:37 +0000  |
|  dune-localfunctions  |  origin/releases/2.8  |  f6628171b2773065ab43f97a77f47cd8c4283d8f  |  2021-08-31 14:03:38 +0000  |

## Installation

The installation procedure is done as follows:
Create a root folder, e.g. `DUMUX`, enter the previously created folder,
clone this repository and use the install script `install_Kiemle2022a.py`
provided in this repository to install all dependent modules.


```sh
mkdir DUMUX
cd DUMUX
git clone https://git.iws.uni-stuttgart.de/dumux-pub/kiemle2022a
./kiemle2022a/install_Kiemle2022a.py
```

This will clone all modules into the directory `DUMUX`,
configure your module with `dunecontrol` and build tests.


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir DUMUX
cd DUMUX
```

Download the container startup script

[docker_kiemle2022a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/kiemle2022a/-/blob/main/docker_kiemle2022a.sh)

to that folder.


Open the Docker Container
```bash
bash docker_kiemle2022a.sh open
```

After the script has run successfully, one may build all executables

```bash
cd kiemle2022a/build-cmake
make build_tests
```

and one can run them individually. They are located in the build-cmake folder according to the following paths:

- appl/1p2c_2p2c_RANS
- appl/1p2c_2p2c_NS

It can be executed with an input file, e.g.

```bash
cd appl/1p2c_2p2c_RANS
./test_stokes1p2cdarcy2p2c_ni_isotope_RANS params_nonisothermal.input
```

