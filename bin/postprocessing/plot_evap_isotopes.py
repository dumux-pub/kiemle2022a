#!/usr/bin/env python

import glob
import sys
import csv
import numpy as np
import os
import copy
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import json
#various style types available, check for good one
plt.style.use('seaborn-whitegrid')
col = plt.rcParams['axes.prop_cycle']

# plot the param study data
fig = plt.figure(dpi=600, figsize=(12,4))

# define two fonts to use for headlines, labels etc
font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 10}

font2 = {'family' : 'normal',
         'weight' : 'bold',
         'size'   : 12}

plt.rc('font', **font)


# get data from txt files
evaporation = np.genfromtxt("add path to input file")
time = evaporation[:,0]/24
y = evaporation[:,1]

#get data from csv files
data_path = "add path to .csv file"
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data = np.array(list(reader)).astype(float)

time2 = data[:,2]/3600/24
H2O18 = data[:,14]
HDO = data[:,15]

data_path2 ="add path to .csv file"
with open(data_path2, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data2 = np.array(list(reader)).astype(float)

time2_2 = data2[:,2]/3600/24
H2O18_2 = data2[:,14]
HDO_2 = data2[:,15]

data_path3 = "add path to .csv file"
with open(data_path3, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data3 = np.array(list(reader)).astype(float)

time2_3 = data3[:,2]/3600/24
H2O18_3 = data3[:,14]
HDO_3 = data3[:,15]


data_path4 = "add path to .csv file"
with open(data_path4, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data4 = np.array(list(reader)).astype(float)

time2_4 = data4[:,2]/3600/24
H2O18_4 = data4[:,14]
HDO_4 = data4[:,15]

fig, axs = plt.subplots(3, sharex=True, sharey=False)
#upper plot
axs[0].plot(time2, H2O18, linestyle='dotted', linewidth=1.5, label='0.001 m')
axs[0].plot(time2_2, H2O18_2, linestyle='dashed', linewidth=1.5, label='0.005 m')
axs[0].plot(time2_3, H2O18_3, linestyle='solid', linewidth=1.5, label='0.009 m')
axs[0].plot(time2_4, H2O18_4, linestyle='dashed', linewidth=1.5, label='0.018 m')

#middle plot
axs[1].plot(time2, HDO, linestyle='dotted', linewidth=1.5)
axs[1].plot(time2_2, HDO_2, linestyle='dashed', linewidth=1.5)
axs[1].plot(time2_3, HDO_3, linestyle='solid', linewidth=1.5)
axs[1].plot(time2_4, HDO_4, linestyle='dashed', linewidth=1.5)

#plot in lower plot
color = next(axs[0]._get_lines.prop_cycler)['color']
axs[2].plot(time, y, linestyle='-', linewidth=1.5,  label='evap. rate', color = color)

#set the labels for y axes
axs[0].set_ylabel(u'$\delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs[1].set_ylabel(u'$\delta^{^2H}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs[2].set_ylabel("Evap. rate [kg/s]", fontsize=font['size'], fontweight=font['weight'])

#limit y axes range
axs[2].set_ylim(0, 0.5e-7)

# Set common label for time
fig.text(0.5, 0.04, 'Time [days]', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center')

axs[0].legend(loc='center left', bbox_to_anchor=(1, 0.5))
axs[2].legend(loc='center left', bbox_to_anchor=(1, 0.5))
fig.tight_layout(rect=[0.01, 0.03, 1, 1])

# place a text box in upper left in axes coords
# these are matplotlib.patch.Patch properties
textstr_a = 'a)\n'
axs[0].text(-0.15, 1, textstr_a, transform=axs[0].transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')
textstr_b = 'b)\n'
axs[1].text(-0.15, 1, textstr_b, transform=axs[1].transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')
textstr_c = 'c)\n'
axs[2].text(-0.15, 1, textstr_c, transform=axs[2].transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')


fig.savefig("evaporation_rate_isotope.png")
fig.savefig("evaporation_rate_isotope.pdf")
#plt.show()
