#!/usr/bin/env python
#change to plot from excel table
import glob
import sys
import csv
import numpy as np
import os
import copy
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import json
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

plt.style.use('seaborn-whitegrid')
col = plt.rcParams['axes.prop_cycle']

# plot the param study data
fig = plt.figure(dpi=600, figsize=(12,8))

font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 12}

font2 = {'family' : 'normal',
         'weight' : 'bold',
         'size'   : 14}

plt.rc('font', **font)


plt.title('H2O18 and HDO composition in the porous medium\n', fontsize=font['size'], fontweight=font['weight'])

# Select paths to .csv input data
# this can be probably solved also with some loops
data_path = "add path to .csv file"
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data = np.array(list(reader)).astype(float)

data_path2 = "add path to .csv file"
with open(data_path2, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data2 = np.array(list(reader)).astype(float)

data_path3 = "add path to .csv file"
with open(data_path3, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data3 = np.array(list(reader)).astype(float)

data_path4 = "add path to .csv file"
with open(data_path4, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data4 = np.array(list(reader)).astype(float)

data_path5 = "add path to .csv file"
with open(data_path5, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data5 = np.array(list(reader)).astype(float)

data_path6 = "add path to .csv file"
with open(data_path6, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data6 = np.array(list(reader)).astype(float)


data_path7 = "add path to .csv file"
with open(data_path7, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data7 = np.array(list(reader)).astype(float)



#Select row for H2O18 and HDO values     
xCoordinate = data[:,1]
DoE0 = data[:,17]
DoE20 = data2[:,17]
DoE40 = data3[:,17]
DoE60 = data4[:,17]
DoE80 = data5[:,17]
DoE100 = data6[:,17]
DoE120 = data7[:,17]

DoE0_1 = data[:,18]
DoE20_1 = data2[:,18]
DoE40_1 = data3[:,18]
DoE60_1 = data4[:,18]
DoE80_1 = data5[:,18]
DoE100_1 = data6[:,18]
DoE120_1 = data7[:,18]

#Plot H2O18 and HDO composition for different times 
fig, axs = plt.subplots(2, sharex=True, sharey=False)

axs[0].plot(xCoordinate, DoE0, linestyle='-', linewidth=2, label='DoE 0')
axs[0].plot(xCoordinate, DoE20, linestyle='-.', linewidth=2, label='DoE 20')
axs[0].plot(xCoordinate, DoE40, linestyle=':', linewidth=2, label='DoE 40')
axs[0].plot(xCoordinate, DoE60,linestyle='-', linewidth=2, label='DoE 60')
axs[0].plot(xCoordinate, DoE80,linestyle='-.', linewidth=2, label='DoE 80')
axs[0].plot(xCoordinate, DoE100,linestyle=':', linewidth=2, label='DoE 100')
axs[0].plot(xCoordinate, DoE120, linestyle='-', linewidth=2, label='DoE 120')

axs[1].plot(xCoordinate, DoE0_1, linestyle='-', linewidth=2, label='DoE 0')
axs[1].plot(xCoordinate, DoE20_1, linestyle='-.', linewidth=2, label='DoE 20')
axs[1].plot(xCoordinate, DoE40_1, linestyle=':', linewidth=2, label='DoE 40')
axs[1].plot(xCoordinate, DoE60_1,linestyle='-', linewidth=2, label='DoE 60')
axs[1].plot(xCoordinate, DoE80_1,linestyle='-.', linewidth=2, label='DoE 80')
axs[1].plot(xCoordinate, DoE100_1,linestyle=':', linewidth=2, label='DoE 100')
axs[1].plot(xCoordinate, DoE120_1, linestyle='-', linewidth=2, label='DoE 120')
ax = plt.gca()


# Set common label for depth
fig.text(0.5, 0.02, 'width [m]', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center')

#set y-labels
axs[0].set_ylabel(u'$\delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs[1].set_ylabel(u'$\delta^{^2H}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])



axs[0].legend(loc='center left', bbox_to_anchor=(1, 0.5))
fig.tight_layout(pad=1, w_pad=0.5, h_pad=1.0)

# place a text box in upper left in axes coords
# these are matplotlib.patch.Patch properties
#textstr = 'b)\n'
#ax.text(-0.2, 2.1, textstr, transform=ax.transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')

fig.savefig("Isotopes_horizontal_all.png")
fig.savefig("Isotopes_horizontal_all.pdf")
