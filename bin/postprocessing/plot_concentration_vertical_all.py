#!/usr/bin/env python
#change to plot from excel table
import glob
import sys
import csv
import numpy as np
import os
import copy
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import json
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

plt.style.use('seaborn-whitegrid')
col = plt.rcParams['axes.prop_cycle']

# plot the param study data
fig = plt.figure(dpi=600, figsize=(12,8))

font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 12}

font2 = {'family' : 'normal',
         'weight' : 'bold',
         'size'   : 14}

plt.rc('font', **font)


plt.title('HDO concentration in the porous medium\n', fontsize=font['size'], fontweight=font['weight'])

# Select paths to .csv input data
# this can be probably solved also with some loops
data_path = "add path to .csv file"
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data = np.array(list(reader)).astype(float)

data_path2 = "add path to .csv file"
with open(data_path2, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data2 = np.array(list(reader)).astype(float)

data_path3 = "add path to .csv file"
with open(data_path3, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data3 = np.array(list(reader)).astype(float)

data_path4 = "add path to .csv file"
with open(data_path4, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data4 = np.array(list(reader)).astype(float)

data_path5 = "add path to .csv file"
with open(data_path5, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data5 = np.array(list(reader)).astype(float)

data_path6 = "add path to .csv file"
with open(data_path6, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data6 = np.array(list(reader)).astype(float)


data_path7 = "add path to .csv file"
with open(data_path7, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    headers = next(reader)
    data7 = np.array(list(reader)).astype(float)

#Select row for H2O18 and HDO values  
yCoordinate = data[:,2]
DoE0 = data[:,17]
DoE20 = data2[:,17]
DoE40 = data3[:,17]
DoE60 = data4[:,17]
DoE80 = data5[:,17]
DoE100 = data6[:,17]
DoE120 = data7[:,17]

DoE0_1 = data[:,18]
DoE20_1 = data2[:,18]
DoE40_1 = data3[:,18]
DoE60_1 = data4[:,18]
DoE80_1 = data5[:,18]
DoE100_1 = data6[:,18]
DoE120_1 = data7[:,18]

#Plot H2O18 and HDO composition for different times 
fig, (axs1, axs2) = plt.subplots(1,2, sharex=False, sharey=True)

axs1.plot(DoE0, yCoordinate, linestyle='-', linewidth=2, label='DoE 0')
axs1.plot(DoE20, yCoordinate, linestyle='-.', linewidth=2, label='DoE 20')
axs1.plot(DoE40, yCoordinate, linestyle=':', linewidth=2, label='DoE 40')
axs1.plot(DoE60, yCoordinate, linestyle='-', linewidth=2, label='DoE 60')
axs1.plot(DoE80, yCoordinate, linestyle='-.', linewidth=2, label='DoE 80')
axs1.plot(DoE100, yCoordinate, linestyle=':', linewidth=2, label='DoE 100')
axs1.plot(DoE120, yCoordinate, linestyle='-', linewidth=2, label='DoE 120')

axs2.plot(DoE0_1, yCoordinate, linestyle='-', linewidth=2, label='DoE 0')
axs2.plot(DoE20_1, yCoordinate, linestyle='-.', linewidth=2, label='DoE 20')
axs2.plot(DoE40_1, yCoordinate, linestyle=':', linewidth=2, label='DoE 40')
axs2.plot(DoE60_1, yCoordinate, linestyle='-', linewidth=2, label='DoE 60')
axs2.plot(DoE80_1, yCoordinate, linestyle='-.', linewidth=2, label='DoE 80')
axs2.plot(DoE100_1, yCoordinate, linestyle=':', linewidth=2, label='DoE 100')
axs2.plot(DoE120_1, yCoordinate, linestyle='-', linewidth=2, label='DoE 120')

ax = plt.gca()
axs1.set_ylim([0.2, 0.4])

# Set common label for depth
fig.text(0.02, 0.5, 'soil column height [m]', fontsize=font['size'], fontweight=font['weight'], ha='center', va='center', rotation='vertical')

#set x-labels
axs1.set_xlabel(u'$\delta^{^{18}O}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])
axs2.set_xlabel(u'$\delta^{^2H}_l$[\u2030]', fontsize=font['size'], fontweight=font['weight'])

axs2.legend(loc='center left', bbox_to_anchor=(1, 0.5))
fig.tight_layout(pad=2, w_pad=1.0, h_pad=1.0)


# place a text box in upper left in axes coords
# these are matplotlib.patch.Patch properties
textstr = 'a)\n'
ax.text(-1.5, 1.08, textstr, transform=ax.transAxes, fontsize=font['size'], fontweight=font['weight'], verticalalignment='top')

fig.savefig("Isotopes_vertical_all.png")
fig.savefig("Isotopes_vertical_all.pdf")
